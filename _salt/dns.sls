#!pyobjects

def record(host, type, value):
    BotoRoute53.present(
        "%s_%s" % (host, type),
        name=host,
        value=value,
        zone="qwertyuiop.ninja",
        record_type=type,
        ttl=(5*60),
    )


def alias(target, source):
    ip4 = salt.dnsutil.A(source)
    if ip4:
        record(target, "A", ip4)

    ip6 = salt.dnsutil.AAAA(source)
    if ip6:
        record(target, "AAAA", ip6)


with BotoRoute53.hosted_zone_present(
    "qwertyuiop.ninja.",
    domain_name="qwertyuiop.ninja.",
    comment="From pairdomains",
):
    alias("qwertyuiop.ninja", "astronouth7303.github.io")
    alias("www.qwertyuiop.ninja", "astronouth7303.github.io")

    record("qwertyuiop.ninja", "MX", [
        "1 ASPMX.L.GOOGLE.COM.",
        "5 ALT1.ASPMX.L.GOOGLE.COM.",
        "5 ALT2.ASPMX.L.GOOGLE.COM.",
        "10 ASPMX2.GOOGLEMAIL.COM.",
        "10 ASPMX3.GOOGLEMAIL.COM.",
    ])

    record("qwertyuiop.ninja", "TXT", [
        '"v=spf1 include:aspmx.googlemail.com ~all"',
        '"google-site-verification=dAJB1jq2AqjybZI9G1oko-9_D2Qf9NdLDOPQJivA5ME"',
        '"keybase-site-verification=6IpkhO1NpKiI0Dwg_nvPgSgU05FFAH5ycVOPUVnPm0g"',
        # Not sure why this was here?
        # '"google-site-verification=ZQmB-dVbn3a8u-uEfMi9VSKQKQpNhRfi4A_yAUAGjps"',
    ])